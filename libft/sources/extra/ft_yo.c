/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_yo.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/31 00:55:14 by gabettin          #+#    #+#             */
/*   Updated: 2019/07/31 01:14:58 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_yo(int i)
{
	static int id;

	if (i < 0)
		id = 0;
	ft_putstr("hey yo : ");
	if (i == 0)
	{
		ft_putnbr(id++);
		ft_putstr(" (id)\n");
		return (id);
	}
	ft_putnbr(i);
	ft_putstr(" (i)\n");
	return (0);
}
