/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_truerandlong.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/17 15:49:16 by gabettin          #+#    #+#             */
/*   Updated: 2019/04/17 15:58:12 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long	ft_truerandlong(long min, long max)
{
	if (max < min)
		return (0);
	if (max == min)
		return (max);
	return ((rand() % (max - min) + min));
}
