/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/03 00:40:32 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:40:44 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *str)
{
	size_t	len;
	int		i;
	char	*result;

	len = ft_strlen(str);
	if ((result = (char*)malloc(sizeof(char) * (len + 1))) == NULL)
		return (NULL);
	len = 0;
	i = 0;
	while (str[i] != '\0')
		result[len++] = str[i++];
	result[len] = '\0';
	return (result);
}
