/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 09:15:15 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:41:24 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s, size_t len)
{
	char	*result;

	if (len == 0)
		return (malloc(0));
	len = ft_strnlen(s, len) + 1;
	if ((result = malloc(sizeof(char) * len)) == NULL)
		return (NULL);
	ft_memset(result, 0, len);
	return (ft_strncpy(result, s, len));
}
