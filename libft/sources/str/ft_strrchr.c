/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 18:42:50 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:41:39 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	const char *last;

	last = s + ft_strlen(s);
	while (last >= s)
	{
		if (*last == (char)c)
			return ((char*)last);
		last -= 1;
	}
	return (NULL);
}
