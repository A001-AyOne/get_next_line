/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 22:00:43 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:41:42 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		*content_process(t_list *t)
{
	char *result;

	if (t == NULL || t->content == NULL)
		return (NULL);
	result = ft_strndup(t->content, t->content_size);
	result[t->content_size - 1] = '\0';
	return (result);
}

static char		*next(char const *s, char c)
{
	while (*s == c)
		s++;
	return ((char *)s);
}

static t_list	*smartcount(char const *s, char c)
{
	t_list	*actual;
	t_list	*first;

	s = next(s, c);
	if (*s == '\0')
		return (ft_lstnew(NULL, 0));
	if ((first = ft_lstnew(s, ft_strclen(s, c) + 1)) == NULL)
		return (NULL);
	actual = first;
	while (*s != '\0')
	{
		if (*s == c)
		{
			s = next(s, c);
			if (*s == '\0')
				return (first);
			if ((actual->next = ft_lstnew(s, ft_strclen(s, c) + 1)) == NULL)
				return (NULL);
			actual = actual->next;
			s = next(s, c);
		}
		else
			s++;
	}
	return (first);
}

char			**ft_strsplit(char const *s, char c)
{
	t_list	*count;
	t_list	*buffer;
	char	**result;

	result = NULL;
	if (s == NULL || (count = smartcount(s, c)) == NULL)
		return (NULL);
	if ((result = (char**)ft_lsttotab(count, content_process)) == NULL)
		return (NULL);
	while (count->next != NULL)
	{
		buffer = count->next;
		free(count);
		count = buffer;
	}
	free(count);
	return (result);
}
