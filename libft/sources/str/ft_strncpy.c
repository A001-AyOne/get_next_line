/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/30 14:48:09 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:41:18 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dest, const char *src, size_t n)
{
	int i;
	int ii;

	i = 0;
	ii = 0;
	while (n-- > 0)
	{
		dest[i] = (src[ii] == '\0') ? '\0' : src[ii++];
		i++;
	}
	return (dest);
}
