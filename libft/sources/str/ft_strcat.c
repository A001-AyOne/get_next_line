/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/31 15:16:50 by gabettin          #+#    #+#             */
/*   Updated: 2019/01/23 16:15:04 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcat(char *s1, const char *s2)
{
	int i;
	int ii;

	i = 0;
	ii = 0;
	while (s1[i] != '\0')
		i++;
	while (s2[ii] != '\0')
		s1[i++] = s2[ii++];
	s1[i] = '\0';
	return (s1);
}
