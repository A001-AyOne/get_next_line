/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printaddr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 08:40:08 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:39:41 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_printaddr_ext(unsigned long int addr, char base[17], char index)
{
	if (addr >= 16)
		index = (char)ft_printaddr_ext(addr / 16, base, index + 1);
	while (index++ < 12)
		ft_putchar('0');
	ft_putchar(base[(addr % 16)]);
	return (12);
}

void		ft_printaddr(void *pointer)
{
	unsigned long int	addr;
	char				base[16];

	base[0] = '0';
	base[1] = '1';
	base[2] = '2';
	base[3] = '3';
	base[4] = '4';
	base[5] = '5';
	base[6] = '6';
	base[7] = '7';
	base[8] = '8';
	base[9] = '9';
	base[10] = 'a';
	base[11] = 'b';
	base[12] = 'c';
	base[13] = 'd';
	base[14] = 'e';
	base[15] = 'f';
	addr = (unsigned long int)pointer;
	ft_printaddr_ext(addr, base, 0);
}
