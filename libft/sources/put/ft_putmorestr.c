/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putmorestr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 08:21:20 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:39:57 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putmorestr(const char *str, char more, size_t final_len)
{
	size_t index;

	index = ft_strlen(str);
	ft_putnstr(str, final_len);
	while (index++ < final_len)
		ft_putchar(more);
}
