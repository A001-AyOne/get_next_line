/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoil.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 05:36:56 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:38:16 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long	ft_atoil(const char *str)
{
	unsigned long	result;
	int				i;
	char			n;

	result = 0;
	i = 0;
	n = 0;
	while (str[i] == ' ' || str[i] == '\t' || str[i] == '\v' || str[i] == '\n'
			|| str[i] == '\r' || str[i] == '\f')
		i++;
	if (str[i] == '-')
	{
		i++;
		n = 1;
	}
	else if (str[i] == '+')
		i++;
	while (str[i] <= '9' && str[i] >= '0')
	{
		result = (result * 10) + (unsigned int)(str[i++] - '0');
		if (i > 19 || (i == 19 && str[i - FT_LONG_MAX]) % 10)
			return (((n == 1) ? 0 : -1));
	}
	return (n == 1 ? (int)-result : (int)result);
}
