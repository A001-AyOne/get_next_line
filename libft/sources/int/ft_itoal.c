/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoal.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 20:03:29 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:38:24 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoal(long n)
{
	char			*result;
	size_t			len;
	unsigned long	b;

	len = (n >= 0) ? ft_digitnbr((int)n) : ft_digitnbr((int)n) + 1;
	result = (char *)malloc(sizeof(char) * (len + 1));
	ft_memset(result, 0, len + 1);
	if (n < 0)
	{
		result[0] = '-';
		b = (unsigned long)-n;
	}
	else
		b = (unsigned long)n;
	while (len-- != ((n >= 0) ? 0 : 1))
	{
		result[len] = (b % 10) + '0';
		b /= 10;
	}
	return (result);
}
