/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 11:26:47 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:38:45 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*result;
	t_list	*actual;

	result = f(lst);
	lst = lst->next;
	actual = result;
	while (lst->next != NULL)
	{
		actual->next = f(lst);
		actual = actual->next;
		lst = lst->next;
	}
	actual->next = f(lst);
	return (result);
}
