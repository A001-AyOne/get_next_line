/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 10:44:00 by gabettin          #+#    #+#             */
/*   Updated: 2019/04/17 14:56:26 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*result;

	if ((result = malloc(sizeof(t_list))) == NULL)
		return (NULL);
	ft_bzero(result, sizeof(t_list));
	result->next = NULL;
	if (content_size == 0 || content == NULL)
	{
		result->content = NULL;
		result->content_size = 0;
		return (result);
	}
	if ((result->content = malloc(content_size)) == NULL)
		return (NULL);
	ft_memcpy(result->content, content, content_size);
	result->content_size = ((content == NULL) ? 0 : content_size);
	return (result);
}
