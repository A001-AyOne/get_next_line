/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lsttotab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 08:30:14 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:38:54 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	**ft_lsttotab(t_list *list, void *(*content)(t_list *t))
{
	void	**result;
	size_t	len;
	size_t	total_len;

	len = 1;
	total_len = (ft_lstlen(list) + 2) * sizeof(void *);
	if ((result = (void **)malloc(total_len)) == NULL)
		return (NULL);
	ft_memset(result, 0, total_len);
	result[0] = content(list);
	while ((list = list->next) != NULL)
		result[len++] = content(list);
	result[len] = content(NULL);
	return (result);
}
