/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 05:45:34 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:39:11 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	if (n == 0)
		return (0);
	return ((((unsigned char*)s1)[0] == ((unsigned char*)s2)[0] && n > 1)
		? ft_memcmp(s1 + 1, s2 + 1, n - 1)
			: ((unsigned char*)s1)[0] - ((unsigned char*)s2)[0]);
}
